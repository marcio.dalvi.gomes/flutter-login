import 'package:flutter/material.dart';
import 'package:login_app/pages/login.page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      title: 'Tela de login',
      debugShowCheckedModeBanner: false,
      theme : ThemeData(
        primarySwatch: Colors.grey,
      ),
      home: LoginPage(),
    );
  }
}